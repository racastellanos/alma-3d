﻿using System.Collections;
using UnityEngine;
using System.Net;
using System;
using System.IO;
using UnityEngine.Networking;
using System.Text;
using SimpleJSON;
using UnityEngine.UI;

public class AssistantController : MonoBehaviour
{
    #region CREDENTIALS
    // Request
    private string apiKey       = "n1cPhk7fB1Liqp0qLG2NO8qH8_EdGD0A6QZ_HmOk-Wrj";
    private string workspace    = "35d25dec-59ff-443f-9ba9-c28af0afb748";
    private string grant_type   = "urn:ibm:params:oauth:grant-type:apikey";
    private string url          = "https://iam.cloud.ibm.com/identity/token";
    private string apiVersion   = "2019-02-28";

    // Header
    private string content_type = "application/x-www-form-urlencoded";
    private string accept       = "application/json";


    private float API_CHECK_MAXTIME = 0; //0 minutes default
    public AudioSource AdalinaSource;
    public InputField textField;
    public Text ConversationText;
    private float apiCheckCountdown = 60 * 60.0f;

    // Authentication
    private string bearer       = "";
    private string assistantResponse = "";

    class IAMAuthentication
    {
        public string access_token;
        public string refresh_token;
        public string token_type;
        public string scope;
        public int expires_in;
        public int expiration;
    }

    void OnEnable()
    {

    }
        #endregion

        public void EnterText()
    {
        GetBearer();
        ConversationText.text = String.Format("Tu: {0}\n\n", textField.text);
        textField.text = "";
    }


    public void GetBearer()
    {
        Debug.Log("en el get bearer");
        //string bearer = "";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("https://iam.cloud.ibm.com/identity/token?grant_type={0}&apikey={1}", grant_type, apiKey));
        request.ContentType = content_type;
        request.Accept = accept;
        request.Method = "POST";
        request.Credentials = new NetworkCredential("apikey", apiKey);

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        IAMAuthentication authentication = JsonUtility.FromJson<IAMAuthentication>(jsonResponse);

        Debug.Log(authentication);

        bearer = authentication.access_token;
        API_CHECK_MAXTIME = authentication.expires_in;

        Debug.Log(bearer);

        StartCoroutine(GetResponseAssistant(textField.text));
    }
    public static void DumpToConsole(object obj)
    {
        var output = JsonUtility.ToJson(obj, true);
        Debug.Log(output);
    }

    public IEnumerator GetResponseAssistant(string text)
    {        
        //var jsonData = JsonUtility.ToJson("{\"input\": {\"text\": \"Hola\"}}", true);
        string dataPass = "{\"input\": {\"text\": \""+ text + "\"}}";
        //Debug.Log("JSON DATA TO PASS");
        //Debug.Log(jsonData);
        UnityWebRequest request = new UnityWebRequest(String.Format("https://gateway-wdc.watsonplatform.net/assistant/api/v1/workspaces/{0}/message?version={1}", workspace, apiVersion), "POST");
        request.method = "POST";
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearer);
        byte[] jsonToSend = new UTF8Encoding().GetBytes(dataPass);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        request.downloadHandler = new DownloadHandlerBuffer();
        yield return request.SendWebRequest();
        Debug.Log("Status Code: " + request.responseCode);

        var jsonResponse = JSON.Parse(request.downloadHandler.text);

        assistantResponse = jsonResponse["output"]["generic"][0]["text"];
        Debug.Log("Status Code: " + assistantResponse);
        ConversationText.text = String.Format("Alma: {0}\n\n", assistantResponse);

        StartCoroutine(GetAudio());
    }

    string authenticate(string username, string password)
    {
        string auth = username + ":" + password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        return auth;
    }
    public IEnumerator GetAudio()
    {
        Debug.Log("Retreiving audio");
        string dataPass = "{\"text\": \"" + assistantResponse + "\"}";

        UnityWebRequest request = UnityWebRequestMultimedia.GetAudioClip("https://gateway-wdc.watsonplatform.net/text-to-speech/api/v1/synthesize?output=text.wav&voice=es-US_SofiaV3Voice", AudioType.WAV);
        
        Debug.Log("Retreiving audio");
        //UnityWebRequest request = new UnityWebRequest(String.Format("https://gateway-wdc.watsonplatform.net/text-to-speech/api/v1/synthesize?output=text.ogg&voice=es-US_SofiaV3Voice"), "POST");
        request.method = "POST";
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "audio/wav");
        request.SetRequestHeader("AUTHORIZATION", authenticate("apikey", "mU54DLo83_JqSpiwferi-rXBcb7TcUWw8t-jAEMPpaqs"));
        byte[] jsonToSend = new UTF8Encoding().GetBytes(dataPass);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        //request.downloadHandler = new DownloadHandlerBuffer();
        yield return request.SendWebRequest();
        Debug.Log("Status Code: " + request.responseCode);
        if (request.isNetworkError)
        {
            Debug.Log(request.error);
        }
        else
        {
            DownloadHandlerAudioClip download = (DownloadHandlerAudioClip)request.downloadHandler;
            download.streamAudio = true;
            AudioClip audioClip = download.audioClip;

            if (audioClip != null)
            {
                Debug.Log("AudioClip downloaded successfully");
                AdalinaSource.clip = audioClip;

                AdalinaSource.Play();
            }

            /**
            AudioClip = DownloadHandlerAudioClip.GetContent(request);
            AdalinaSource.clip = AudioClip;
            AdalinaSource.Play();
            /**/
        }

        //string assistantResponse = jsonResponse["output"]["generic"][0]["text"];
    }
}
